# DevOps Anthology

## Description

A series of mini courses starting with Linux and leading toward more advanced DevOps skills.

## Information

| Item | Value |
| --- | --- |
| Start Date | 2022-05-05 |
| Overview | [Link](https://class.mastermnd.io/devops) |
| Waddle: Foundations in Linux | 7 sessions |
