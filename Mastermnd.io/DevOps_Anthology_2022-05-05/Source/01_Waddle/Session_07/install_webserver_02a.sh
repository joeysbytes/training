#!/bin/bash

DISTRO=$1

if [[ $DISTRO == "ubuntu" ]]; then
        echo "You're running Ubuntu, using apt to install nginx"
        apt install nginx && systemctl start nginx
elif [[ $DISTRO == "redhat" ]]; then
        echo "You're running RedHat, using yum to install apache"
        yum install httpd && systemctl start httpd
fi

echo "DONE!!!"
