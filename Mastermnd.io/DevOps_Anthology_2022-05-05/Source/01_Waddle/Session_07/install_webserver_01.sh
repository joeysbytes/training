#!/bin/bash

echo "Getting ready to install nginx"
sleep 3

# Install and Start Nginx
apt install nginx && systemctl start nginx

sleep 1

echo "NGINX has been installed and started!!!"
