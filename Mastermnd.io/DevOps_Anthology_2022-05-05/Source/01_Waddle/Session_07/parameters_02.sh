#!/bin/bash

NAME=$1
AGE=$2
KARATE_BELT_LEVEL=$3

echo "My name is $NAME and I am $AGE years old and I am a $KARATE_BELT_LEVEL belt!"
