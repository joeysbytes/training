# DevOps Anthology: Waddle Session 6 - 2022-06-09

## Foundations In Linux: Linux Networking Concepts

* What is the internet?
  * Global system of interconnected computer networks
  * Uses the Internet Protocol suite (TCP/IP) to communicate between networks and devices
    * IP = Internet Protocol
* TCP/IP
  * Suite of protocols to communicate over a network
  * IP - Internet Protocol
    * Part that obtains the address of where data will be sent
  * TCP - Transmission Control Protocol
    * Responsible for delivering data to the IP Address
* Network Interfaces
  * A machine can have multiple network interfaces
  * ip, ifconfig
* You can trace / track the path of packets
  * tracepath, traceroute, mtr
* How the internet works
  * DNS - Domain Name System
    * Like a phonebook for the internet
    * Web browsers use ip addresses, but we use DNS names. DNS does this translation.
    * Caching is involved
  * DNS Record Types
    * A - Host record, IPv4, most common
    * AAAA - Same as A record, but IPv6
    * CNAME - Canonical name, domain name aliases
    * MX - Mail Exchange (email servers)
    * NS - Name Server, server responsible for DNS info
    * There are many more types
  * dig, nslookup, whois, /etc/hosts, /etc/resolv.conf
* Call Forwarding
  * Can use host file to intercept / override DNS mapping
  * /etc/hosts
  * Computer always looks here first, before querying DNS
* Connections and Sockets
  * You can get info about connections on your system
    * Ports and sockets being used
  * Only one service can use any port at any time
  * lsof, netstat, /etc/services

## Commands

* ip: shows information about network interfaces
  * ip addr
    * lo - loopback interface
      * allows system to speak to itself
      * always 127.0.0.1
      * this is also localhost
    * You can do many things with this command
      * Bring interfaces up and down
* ifconfig: older, getting deprecated, use ip command
* tracepath
  * simpler than traceroute
* traceroute
* mtr
  * like the htop of traceroute / tracepath
  * displays information nicely
* dig - to query DNS info
* cat /etc/services
  * has list of all known standard services / ports
* lsof - list open files
  * lsof -i -P
* netstat - another tool like lsof
  * netstat -tulpn
* curl -I
  * Will just show status of request

## Homework Links

- [x] [Session 6 Slides](https://docs.google.com/presentation/d/1x2-ZCV5-AC_LXMQSbsbRwpgTlnlwEOKtk6BnB7nxFaM/edit?usp=drive_web&authuser=0)
- [ ] [7 Linux networking commands that every sysadmin should know](https://www.redhat.com/sysadmin/7-great-network-commands)
- [ ] [‘ip’ Command cheat sheet](https://www.thegeekdiary.com/ip-command-cheat-sheet-command-line-reference)
- [x] [MX Toolbox DNS Lookup](https://mxtoolbox.com/DNSLookup.aspx)

## Other Links

* [Advent of Code](https://adventofcode.com/)
* [Regex Adventure](https://goldenlion5648.itch.io/regex-adventure)
