# DevOps Anthology: Waddle Session 2 - 2022-05-12

## Foundations In Linux: Navigation

* Files and Systems
  * Everything is a file.  If not a file, it's a process.
  * Tree format
  * Everything starts at '/'
* GUI - Graphical User Interface
* CLI - Command-Line Interface
* Shells
  * Interprets your input and executes appropriate actions (CLI)
  * Different shells have different tools, features, and ways of doing things
    * sh - Bourne Shell, original / default Unix shell, guaranteed to be there
    * bash - Bourne Again Shell, most popular
    * zsh - Extended version of Bourne Shell, becoming default on some Linux distros, and Mac
      * See tool: Oh My Zsh
    * Many more shells, like ksh, tcsh, csh, fish, etc.
* Paths
  * Absolute file paths - always starts at root, and must start with /
  * Relative file paths - starts at current path, cannot start with /
* CLI Navigation (hands-on)
* Command-Line Arguments and/or Flags
* Hidden files in Linux start with a .
* Tab completion
* Reverse search
  * Start typing a command, press Ctrl+R, it helps you find previous command
* Up and Down arrows to cycle through commands

## Commands

* pwd - present working directory
* ls - list directory contents
  * -l - long listing
  * -F - puts ending slashes on directories (or other indicators)
  * -a - show all files (hidden)
* cd - change directory
  * .. = parent directory
  * . = current directory
* mkdir - make a directory
* history - shows list of commands you have run
  * !## - run history command at number ##
* man - the manual (man pages)
* tree - shows tree structure

## Homework / Links

- [x] [Session 2 Slides](https://docs.google.com/presentation/d/1XCO5jBkTjLJF-d2_duEVV9hzR3jzyMwAv1J07TGunvE/edit?usp=drive_web&authuser=0)
- [x] [Linux File System/Structure Explained!](https://www.youtube.com/watch?v=HbgzrKJvDRw)
- [x] [Beginner: Linux Navigation Manual](https://www.pluralsight.com/guides/beginner-linux-navigation-manual)
- [x] [Linux Commands Cheat Sheet: With Examples](https://phoenixnap.com/kb/linux-commands-cheat-sheet)

## Links

* [Unix Porn](https://www.reddit.com/r/unixporn/)
* [Indeed](https://www.indeed.com/)
* [Explain Shell](https://explainshell.com/) - enter a command, get an explanation of all its parts
* [tldr pages](https://tldr.sh/) - gives easier to read man pages (community contributed)
