# DevOps Anthology: Waddle Session 7 - 2022-06-16

## Foundations In Linux: Scripting in Linux

* What is a script? - code that automates tasks
* Base Skills
  * Arguments / Parameters - position matters
  * shebang
  * Scripting is Programming
  * Conditional Execution
  * Comparison Operators
  * Logical Operators
  * Exit Codes
  * Loops

## Commands

* wc - word count

## Labs

* Lab 1: Install nginx and start it on a server
* Lab 2: Modify script to install apache if on RedHat distribution
* Lab 3: (skipped) - Make a helper script to add/delete users
  * Look up how to write a usage statement
* Bonus Lab: Did first part, next 2 parts for us to do.
@ 2h 50m

## Homework Links

- [x] [Session 7 Slides](https://docs.google.com/presentation/d/1OrNVkHNU40Xdd6CDBzBlGczSeUHmRLSzAE4vAsovSXw/edit?usp=drive_web&authuser=0)
- [ ] [Another Bash Scripting Lab](https://decal.ocf.berkeley.edu/labs/b3/)
- [ ] [What is "the Shell"?](http://linuxcommand.org/lc3_lts0010.php)
- [x] [Bash script labs source code](https://github.com/mastermndio/2022-bash-script-lab)
- [ ] Lab Part 3 is left for us to do on our own.
- [ ] Bonus Lab Part 2 and 3 is left for us to do on our own.

## Other Links

* [Roadmap](https://roadmap.sh/) - charts to help you get to a specific role.
* [GitHub CLI](https://cli.github.com/manual/) - gh command
* [nvm.sh](https://github.com/nvm-sh/nvm/blob/master/install.sh) - shell script that automates node.js installation
