# DevOps Anthology: Waddle Session 3 - 2022-05-19

## Foundations In Linux: Users and Group Management

* 2 Types of Users
  * Normal users
  * Super User(root)
* List Users: cat /etc/passwd
  * Example: joey:x:1000:1000:Joey Rockhold:/home/joey:/bin/bash
  * Example: mastermnd:x:1001:1001:Aaron B,1,111,222,123:/home/mastermnd:/bin/bash
  * Format: username:?:uid:gid:full name,room,work num,home num,other:home dir:shell
* List groups: cat /etc/group
  * Example: joey:x:1000:user1
  * Format: username:?:gid:other users
* Ctrl + L = clear screen
* Ctrl + R = reverse search bash history
* ~ = shortcut for home directory
* $OLDPWD = holds last directory you were in
* Long Listing
  * Example: -rw-r--r--  1 root root    0 May 20 23:50 ourfile.txt
  * Format: file permissions | owner user | owner group | size | last modified date | file name
* nano - simple, accessible text editor
  * did a very brief overview
* Permissions
  * 3 permissions
    * r - read (4)
    * w - write (2)
    * x - execute (1)
  * 3 applications
    * user owner
    * group owner
    * other / everyone else

## Commands

* adduser, useradd: add a user
* clear: clear the screen
* deluser, userdel: delete a user
* addgroup, groupadd: add a group
* delgroup, groupdel: delete a group
* usermod: modify a user
  * usermod -a -G groupname username: add a user to a group
* pwd: present working directory
* cd: change directory
  * cd: by itself, goes to home directory
  * cd ~: goes to home directory
  * cd -: goes to the last directory you were in
  * cd "${OLDPWD}": goes to the last directory you were in
* ls: list files
* mkdir: create a directory
* touch: create empty file and/or update timestamp
* stat: give detailed info about a file
* cat: concatenate files and prints to standard output (stdout)
* more: scrolling interface to view a file
  * spacebar: page down
  * enter: go down a line
* less: like more, but more advanced
  * arrows, pgup, pgdn, vi movement keys, vi searching, etc
* head: display top # of lines in a file, default = 10
* tail: display bottom # of lines in a file, default = 10
  * tail -f filename: constantly shows the bottom of a file
* cp: copy
* mv: move (or rename)
* grep: string pattern matcher, can use regex
  * cat /etc/group | grep messagebus = grep messagebus /etc/group
  * grep -r messagebus /etc/*
* find: search for files and directories
* chown: change owner (can also change group)
* chgrp: change group
* chmod: change mode (permissions)
* batcat: cat on steroids, formats, syntax highlighting, less

## VI/VIM

* Modal text editor
* Steep learning curve
* Feature rich
* Big plugin ecosystem
* Fast
* Modes
  * Normal Mode
    * default
    * used for navigation
    * ESC brings you here
  * Insert Mode
    * You type in this mode
  * Command Mode
    * You enter commands
  * Replace Mode
    * Replace text your cursor is on
* Navigation - you do not use your mouse
  * j = up
  * k = down
  * h = left
  * l = right
  * can also use arrow keys
* VIM commands
  * :w = write (save) the file
  * :q = quit
  * ! = force the command
  * :wq = write and quit
  * ZZ = another way to save and quit
  * ZQ = another way to quit without saving
  * i = insert mode
  * r = replace mode

## Homework Links

- [x] [Session 3 Slides](https://docs.google.com/presentation/d/1GGOtGGJzfcXG2T0o8-Gl7HE_VBNNimWc-87moMj9KUk/edit?usp=drive_web&authuser=0)
- [x] [VIM Adventures](https://vim-adventures.com/)
