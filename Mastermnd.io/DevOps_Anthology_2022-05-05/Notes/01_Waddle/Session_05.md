# DevOps Anthology: Waddle Session 5 - 2022-06-02

## Foundations In Linux

### Process Management - What's Running

* SIGTERM: request to program to terminate

#### Commands

* ps: report a snapshot of the current processes
  * PID: Process ID
  * TTY: Teletype device
  * CMD: Command
  * Time: Time process has taken
* top: display linux processes live
  * load average
    * 1, 5, and 15 minute average
    * 1.00 = 100% utilized
    * Workload / number of cores
  * PR: Priority, lower number = more priority
  * NI: Nice, also a priority
  * See Linux article linked below for good description of what top is showing you
  * Has hotkeys while observing processes
* htop: Nicer version of top, colors, more tools, mouse support
* kill: Sends a termination signal to a process
* pkill: like kill, but goes by process name instead of PID
* free: shows memory usage
* df: Disk file system information
* du: Disk usage information

### System Startup

* Run Levels (modes)
  * 0 - Halt
    * system shutdown
  * 1 - Single-User Mode
    * god mode/root user
    * meant for system rescue
    * no networking
  * 2 - Multi-User Mode
    * can log in as users other than root
    * no networking
  * 3 - Multi-User Mode with Networking (normal startup)
    * linux servers normal boot mode
  * 4 - Undefined
    * you can actually set this up on your own
  * 5 - X11
    * GUI mode
    * display manager runs
  * 6 - Reboot
    * system reboot
* System services
  * old commands were service, init.d

#### Commands

* Get current runlevel
  * runlevel
  * who -r
  * systemctl get-default
* Change runlevel
  * init #
  * systemctl isolate *option.target*
  * systemctl set-default graphical.target  (permanent change)
* reboot: reboot a server
  * reboot now
* What starts at boot
  * systemctl list-units
  * systemctl enable *servicename.service*
  * systemctl disable *servicename.service*
  * systemctl start *servicename.service*
  * systemctl stop *servicename.service*
  * systemctl status *servicename.service*
  * systemctl reload *servicename.service*  (to reload configuration changes, quicker than restart)
  * systemctl restart *servicename.service*
* /etc/rc.*

### Cron

* Used to schedule commands
  * /etc/cron.daily
  * /etc/cron.monthly
  * /etc/cron.weekly
  * /etc/crontab
  * crontab -e

```text
+----------- minute (0 - 59)
| +--------- hour (0 - 23)
| | +------- day of month (1 - 31)
| | | +----- month (1 - 12)
| | | | +--- day of week (0 - 6)
| | | | |
* * * * *
```

## Homework Links

- [x] [Session 5 Slides](https://docs.google.com/presentation/d/1BiNxL3_bxDYPhbYH5s28sNw9OPYF8WPjR1ylAqsA9Os/edit?usp=drive_web&authuser=0)
- [x] [Crontab Guru](https://crontab.guru/)
- [x] [Commands for Process Management in Linux](https://www.journaldev.com/43930/process-management-in-linux)
- [x] [Termination Signals](https://www.gnu.org/software/libc/manual/html_node/Termination-Signals.html)
