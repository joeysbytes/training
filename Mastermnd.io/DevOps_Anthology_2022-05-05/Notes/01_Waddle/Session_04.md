# DevOps Anthology: Waddle Session 4 - 2022-05-26

## Foundations In Linux: Package Management

* What is a package?
* What is a package manager?
  * Distro-based packaging
    * Debian, Ubuntu
      * dpkg - Debian (install/uninstall .deb packages)
      * apt - advanced package tool (uses dpkg, also handles dependencies)
      * apt-get: older version of apt, deprecated
    * Redhat
      * yum - yellowdog updater, modified
      * dnf - Danfield yum (better dependency management, memory optimization, python3 support)
* What are dependencies?

## Commands / Follow-Along

### Ubuntu 22.04

* Run Updates
  * apt update: refreshes repository database
  * apt upgrade: updates the packages
  * reboot now
* Install tree manually
  * curl -o tree.deb http://archive.ubuntu.com/ubuntu/pool/universe/t/tree/tree_2.0.2-1_amd64.deb
  * dpkg -i tree.deb: install tree
  * dpkg -r tree: remove tree
* Install tree with package manager
  * apt install tree
  * apt remove tree
* See installed packages
  * dpkg -l
  * apt list --installed
* Search for packages
  * dpkg-query -S tree
  * apt search tree
  * https://www.debian.org/distrib/packages
* Add a repository
  * add-apt-repository ppa:longsleep/golang-backports
  * apt update
  * apt install golang-go
  * ls /etc/apt/sources.list.d/
* Build neovim from source: https://github.com/neovim/neovim/wiki/Building-Neovim
  * apt install build-essential
  * sudo apt-get install ninja-build gettext libtool libtool-bin autoconf automake cmake g++ pkg-config unzip curl doxygen
  * cd
  * git clone https://github.com/neovim/neovim
  * cd neovim
  * make
  * make install
* Build Ruby from source: https://www.ruby-lang.org/en/documentation/installation/#building-from-source
  * cd
  * wget https://cache.ruby-lang.org/pub/ruby/3.1/ruby-3.1.2.tar.gz
  * tar -xvf ruby-3.1.2.tar.gz
  * cd ruby-3.1.2
  * ./configure
  * make
  * make install

### CentOS Stream 9

**Note:** VirtualBox kept crashing trying to run CentOS Stream 9, so I installed Fedora Server 36
instead, it all works out the same.

dnf replaces yum, but both still work

* Run Updates
  * yum update
* Install tree manually
  * curl -o tree.rpm https://rpmfind.net/linux/fedora/linux/releases/36/Everything/x86_64/os/Packages/t/tree-2.0.1-2.fc36.x86_64.rpm
  * yum localinstall tree.rpm
* Install tree
  * yum install tree
  * yum remove tree
* See installed packages
  * yum list --installed
  * dnf history userinstalled
* Search for packages
  * yum search tree
  * dnf search tree
* Build neovim from source: https://github.com/neovim/neovim/wiki/Building-Neovim
  * yum groupinstall "Development Tools"
  * yum -y install ninja-build libtool autoconf automake cmake gcc gcc-c++ make pkgconfig unzip patch gettext curl
  * cd
  * git clone https://github.com/neovim/neovim
  * cd neovim
  * make
  * make install

### Other / Misc

* Find what distribution / version you are on
  * cat /etc/redhat-release
  * cat /etc/issue
  * uname -a

## Homework Links

- [x] [Session 4 Slides](https://docs.google.com/presentation/d/1blUFz6hg7AQsy8ErMOJa_prTHTHJ2nKQ68p8WVH2trU/edit?usp=drive_web&authuser=0)
- [x] [APT Cheat Sheet](https://blog.packagecloud.io/apt-cheat-sheet/)
- [x] [YUM Command Cheat Sheet](https://access.redhat.com/sites/default/files/attachments/rh_yum_cheatsheet_1214_jcs_print-1.pdf)
- [ ] [How to Compile and Install Software From Source in Linux](https://www.makeuseof.com/compile-install-software-from-source-linux/)

## Other Links

* [Notion](https://www.notion.so/) - Online Notes
* [Evernote](https://evernote.com/) - Online Notes
* [DigitalOcean Functions](https://www.digitalocean.com/products/functions)
* [RPM Find](https://rpmfind.net/)
