# DevOps Anthology: Waddle Session 1 - 2022-05-05

## Foundations in Linux

* What is Linux?
  * What is a kernel?
  * What are distributions?
    * Debian based
      * Ubuntu based
    * Redhat based
    * Opensuse based
    * Arch based
    * Specialty distros
* Virtualization
  * Hypervisors
    * Type 1 - Bare Metal
    * Type 2 - Hosted
  * Virtual Machines
  * Containers
* Linux Installation Options
  * Linode (MasterMnd has 60-day free code for new users)
    * Ubuntu 22.04 LTS install demo'd
    * Nanode 1 GB size ($5 / month)
    * Password login for now
    * Select private IP
    * Tested login, logout of root user
  * Virtualization
    * VirtualBox
    * WSL2
      * Installed WSL2
      * Installs Ubuntu by default
  * Cygwin - would work for the basics
  * Dual Boot
  * Native Install

## Homework

* [x] Have a linux installed that you will use for the course.

## Links

* [Unix Porn](https://www.reddit.com/r/unixporn/)
* [Indeed](https://www.indeed.com/)
