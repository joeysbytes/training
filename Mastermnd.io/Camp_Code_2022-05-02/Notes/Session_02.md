# Camp Code: Session 2 - 2022-05-09

## Environment Setup

* What is a development environment?
  * Also local, staging, live (production)
* What is a command line interface?
  * Showed starting command prompt, powershell, WSL
  * Showed Mac Terminal
  * Showed Linux Terminal
* Showed installing Go on all platforms
  * Also showed installing manually on linux
* What is a text editor?
* What is an IDE? - Integrated Development Environments
* What is Visual Studio Code?
* Showed installing VS Code on Windows
  * Let VS Code install Go extensions
* Showed VS Code features, overview of the layout, themes, terminal

## Go Language

* Need a "package main" and a "func main() {}" in every Go program.
* To run go program: _go run programname.go_
  * Compiles behind the scenes and runs it
* To build executable: _go build programname.go_

## Homework Links

- [x] [Session 2 Slides](https://docs.google.com/presentation/d/1ftc7psdGP1RJAm8ZcPB9dadBz7ttFmE4BpiBLiQ__IY/edit?usp=drive_web&authuser=0)
- [x] [COMPILER| INTERPRETER |Difference between Interpreter and Compiler| Interpreter vs Compiler Animated](https://www.youtube.com/watch?v=e4ax90XmUBc)
- [x] [How Compilers Work](https://www.youtube.com/watch?v=xLuKxomEUiQ)
- [x] [How does a compiler, interpreter, and CPU work?](https://www.youtube.com/watch?v=OVTu4XcmnwE)
- [x] [Let's Build a Compiler! LIVE](https://www.youtube.com/watch?v=jeOU6KJFi8c)
- [x] [main and init function in Golang](https://www.geeksforgeeks.org/main-and-init-function-in-golang/#:~:text=In%20Go%20language%2C%20the%20main,any%20argument%20nor%20return%20anything.)
- [x] [GoDocsMain.png](https://drive.google.com/file/d/1p2sL_9thBQtrY5T-1OY8Uc1ZxFREqrtX/view?usp=drive_web&authuser=0)
- [x] [Visual Studio Code Crash Course](https://www.youtube.com/watch?v=WPqXP_kLzpo)
- [x] [Quiz 1](https://docs.google.com/forms/d/e/1FAIpQLSd_wr_0kpRPC4KqkOYsvVOQ6PxwqK5EldmQW0jWXy39W0gaxQ/viewform?hr_submission=ChkI9eyunuIHEhAI2aL3t5UOEgcIkaWP3u0OEAE&authuser=0)

## Other Homework

- [x] Make sure Go is installed and working (go version)
- [x] Have Visual Studio Code installed
- [x] Play with Go

## Other Links

* [replit](https://replit.com/) - in browser IDE
* [Go Playground](https://go.dev/play/)
* [Coder](https://coder.com/) - self-hosted developer workspaces
* [Go Installation Instructions](https://go.dev/doc/install)
* [GitHub CodeSpaces](https://github.com/features/codespaces)
* [Hyper](https://hyper.is/) - Fun Terminal
  * [gitrocket](https://github.com/bomanimc/gitrocket)
* [Visual Studio Code](https://code.visualstudio.com/)
* [Homebrew](https://brew.sh/) - Package manager for Mac
* [Chocolatey](https://chocolatey.org/) - Package manager for Windows
