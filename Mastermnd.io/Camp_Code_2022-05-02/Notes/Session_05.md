# Camp Code: Session 5 - 2022-06-06

## #StringTheory

* Strings
  * Sequence of characters
  * When you see double quotes, think string
  * Part of Strings package (standard library)
* Strings Package Documentation
  * Looked at the documentation page, skimmed available functions, how to read it
  * You can run code in the docs
* Concatenation
  * Joining strings together end-to-end
  * "String1" + "String2" = "String1String2"
  * Templating using fmt.Sprintf()
* Interpolation
  * Process of substituting values of variables in to placeholders in a string
  * Variables inside of strings
  * "Hello my name is first_name last_name"
  * fmt.Printf() - prints to stdout (no newline added automatically)
  * fmt.Sprintf() - returns resulting string
* String functions
  * String slicing: "Aaron"[2] = "r" (this is a rune)

## Other Notes

* Go is a "class-less" language
* Go uses zero-based indexing
* rune is the same as "char" in other languages
* Maps are meant to iterate over all items in a list, and return a new list.

## Homework Links

- [x] [Session 5 Slides](https://docs.google.com/presentation/d/1Fq-FGk-r0esP7j49YkCnpqAcCD5krntdHioumvKY-WI/edit?usp=drive_web&authuser=0)
- [x] [strings Package](https://pkg.go.dev/strings)
- [x] [fmt Package](https://pkg.go.dev/fmt)

## Other Links

* [Oh My Bash](https://github.com/ohmybash/oh-my-bash)
* [Exorcism](https://exercism.org/) - Coding practice exercises, tries to mimic real-world problems.
