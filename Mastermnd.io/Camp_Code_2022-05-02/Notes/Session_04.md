# Camp Code: Session 4 - 2022-05-23

## Running Code, Data Types Intro, and Displaying Output

* imports: Bring in packages to our program
* gofmt: Go automatically formats the structure of your code, assuming syntax is correct
  * The VSCode extension will call this for you on save
  * To run manually: go fmt filename.go

## Data Types

* Data Types
  * bool
  * string
  * int, int8, int16, int32, int64
  * uint, uint8, uint16, uint32, uint64, uintptr
  * byte (same as uint8)
  * rune (same as int32)
  * float32, float64
  * complex64, complex128
* Strings
  * list of characters
  * double quotes = string
  * single quotes = not a string
* Ints / Uints - Integers
  * Signed / Unsigned
  * int = int32 or int64 depending on system
  * uint = uint32 or uint64 depending on system
* Floats - Decimals
* Runes - single character
  * alias for int32
  * references a character
  * used to emphasize the ASCII/Unicode encoding of a character
  * single quotes, single character
* Bytes
  * alias for unit8
  * references a character
  * 0 - 255

## Go Language Commands

* go
  * shows quick help screen
* go version
  * shows go version installed
* go build filename.go
  * compilation to executable
  * target: the architecture you are building for (defaults to machine you are on)
    * You can change this
* go run filename.go
  * does a rapid compilation and runs it
  * caching is involved
* go fmt filename.go
  * format file to go standards

## Displaying Output

* fmt.Print (no newline output)
* fmt.Println (newline output)
* fmt.Printf (no newline output, string formatting)
* Special / Escape Characters:
  * \n - newline
  * \t - tab
  * \v - vertical tab
  * \r - carriage return
  * \\ - backslash

## Other Notes

* Go emphasizes building things yourself
* Standard Output is your console / screen
* 1.18.2 - Symantic Versioning: Major.Minor.Patch
* Discussed what variables are
  * Go does not allow you to define a variable and not use it.
  * You do not have to give a variable an initial value, they have "zero values" (defaults).
* Discussed what constants are (use 'const' keyword, otherwise same syntax as variables)
* Discussed what comments are
* nil = undefined value (just like null, None, etc.)
* Very brief introduction to string templating / formatting

## Code Snippets

```go
// get first letter of a string
fmt.Println("Hello"[0])          // 72
fmt.Println(string("Hello"[0]))  // H
```

<hr />

```go
// USING VARIABLES

// Explicit declaration followed by assignment
var name string
name = "Joey"

// Explicit declaration and assignment
var name string = "Joey"

// Implicit declaration and assignment (most common usage)
name := "Joey"
```

## Homework Links

- [x] [Session 4 Slides](https://docs.google.com/presentation/d/1koSkp9N0YIIpLSLa4IbL_6L__qRHW69QfGmKbJ29SEc/edit?usp=drive_web&authuser=0)
- [x] [Go Playground](https://go.dev/play/) - Online Go Editing / Running
- [x] [Go Docs Home](https://go.dev/doc/)
- [x] [Go Packages Docs](https://pkg.go.dev/std)
- [x] [Oh Shit, Git!?!](https://ohshitgit.com/) - Git recipes for when you screw up
  - [x] [Dangit, Git!?!](https://dangitgit.com/en) - Family friendly version of the site
- [x] [Go By Example](https://gobyexample.com/) - Great site if learning Go, but already know how to program

## Other Homework

* Last week's and this week's classroom stuff was published to the wrong place, expect to see it soon.
