# Camp Code: Session 1 - 2022-05-02

## Syllabus Day + Intro to Programming

* What is computer science?
* What is programming?
* How do we give computers instructions?
  * How do computers understand our instructions?
    * Source code
      * Compiler
      * Interpreter
      * Byte Code
    * End result is always: Machine Code
      * Discussed binary
* Data Types
* Static Typing
* Dynamic Typing
* We will be learning with Go (or Golang)
* How do we write code?
  * Recommended tool: Visual Studio Code

## Homework Links

- [x] [Session 1 Slides](https://docs.google.com/presentation/d/1gff4b90MKJZVyq3SCkmR1NaaPc3LAXKsjrlnRs9pPDs/edit?usp=drive_web&authuser=0)
