# Camp Code: Session 3 - 2022-05-16

## Command Line Introduction

* This is a review from DevOps Anthology last Thursday, I'll make notes if anything new is said.
  * The differences in Windows is pointed out.

## Linux Commands

* cd
* mkdir / rmdir
* pwd
* ls (Windows: dir)
* touch
* cat (Windows: type)
* more / less
* head / tail
* cp (Windows: copy)
* mv

## Go Language

## Homework Links

- [x] [Session 3 Slides](https://docs.google.com/presentation/d/16qc3myG0BKrl2vsX-02I8n6khikTPjc5eslpWmeQ-oU/edit?usp=drive_web&authuser=0)

## Other Links

* [Go Standard Library Solutions Video](https://www.packtpub.com/product/go-standard-library-solutions-video/9781788474160)
