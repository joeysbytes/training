package main

import "fmt"

func main() {
	fmt.Print("Hello World")
	fmt.Println("Hello World")
	fmt.Printf("Hello World")

	fmt.Print("\tHello World\n")
	fmt.Println("Hello \\ World")

	fmt.Println("Bobby said your code doesn't work because you are missing a quote.\"")

	// Demo string templating / formatting
	name := "Joey"
	age := 55
	height := "7ft"

	fmt.Printf("My name is %v and I am %v years old. I am also %v tall.\n", name, age, height)

}
