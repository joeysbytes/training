package main

import "fmt"

func main() {
	// This is a comment. It is ignored by the compiler.
	// Our first variable

	// var age int
	// age = 32

	// var age int = 32

	age := 32
	fmt.Println(age)
	age = 55
	fmt.Println(age)

	const years int = 33
	fmt.Println(years)
	// years = 66  // error
	// fmt.Println(age)

}
