package main

import "fmt"

func main() {
	var firstName string
	firstName = "Joey"

	lastName := "Rockhold"

	// fullName := fmt.Sprintf(firstName + " " + lastName)
	fullName := fmt.Sprintf("%v %v", firstName, lastName)

	fmt.Println("Hello, my name is " + fullName + ", and I am awesome!")

	fmt.Printf("Hello, my name is %v, and I am awesome!\n", fullName)

}
