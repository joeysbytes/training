package main

import (
	"fmt"
	"strings"
)

func main() {
	// Imagine we are collecting email addresses for a mailing list
	// In email, case does not matter

	// Normalize by making this all lower-case
	email := "CoolPerson.Billy@email.com"
	normalized_email := strings.ToLower(email)
	// normalized_email := strings.ToUpper(email)
	fmt.Println(normalized_email)

}
