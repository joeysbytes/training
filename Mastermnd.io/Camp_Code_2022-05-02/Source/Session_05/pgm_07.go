package main

import (
	"fmt"
	"strings"
)

func main() {
	// Imagine we are collecting street addresses to go in to a database.
	// Let's split up the information to make it easier to organize it all.

	address := "1234,FancyHouse Ln,Pittsburg,PA,20932"
	processed_address := strings.Split(address, ",")
	fmt.Println(processed_address)

	houseNum := processed_address[0]
	street := processed_address[1]
	city := processed_address[2]
	state := processed_address[3]
	zip := processed_address[4]

	fmt.Printf("%v %v\n%v, %v %v\n", houseNum, street, city, state, zip)
	fmt.Printf("%#v", processed_address)

}
