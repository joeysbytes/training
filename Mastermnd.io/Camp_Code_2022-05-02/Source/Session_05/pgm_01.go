package main

import "fmt"

func main() {
	var firstName string
	firstName = "Joey"

	lastName := "Rockhold"

	// Print parts of name on separate lines
	fmt.Println(firstName)
	fmt.Println(lastName)

}
