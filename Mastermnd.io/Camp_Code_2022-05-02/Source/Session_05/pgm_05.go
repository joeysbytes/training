package main

import "fmt"

func main() {
	var firstName string
	firstName = "Joey"

	fmt.Println(firstName[0])         // 74
	fmt.Println(string(firstName[0])) // J

	for i := 0; i < len(firstName); i++ {
		fmt.Println("The next letter in the string is:", string(firstName[i]))
	}

}
