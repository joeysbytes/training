package main

import "fmt"

func main() {
	var firstName string
	firstName = "Joey"

	lastName := "Rockhold"

	fullName := firstName + " " + lastName

	fmt.Println("Hello, my name is", fullName, ", and I am awesome!")
	fmt.Println("Hello, my name is " + fullName + ", and I am awesome!")

	fmt.Printf("Hello, my name is %v, and I am awesome!\n", fullName)

	// more examples
	fmt.Printf("21 converted to binary is: %b\n", 21)
	fmt.Printf("Data Type: %T\n", fullName)
	fmt.Printf("Data Type: %T\n", 123)
	fmt.Printf("Data Type: %T\n", 3.14)

}
