package main

import "fmt"

func main() {
	var firstName string
	firstName = "Joey"

	lastName := "Rockhold"

	// Print name parts on single line

	// fullName := firstName + lastName
	// fullName := firstName + " " + lastName
	fullName := firstName + " " + lastName + " is the coolest person on the " + "PLANET"
	fmt.Println(fullName)

}
