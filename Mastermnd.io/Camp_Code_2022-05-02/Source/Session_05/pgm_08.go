package main

import (
	"fmt"
	"strings"
)

func main() {

	// Check if March 21 is the person's birthday, if so they win a prize.
	birthday := "March 21, 1990"
	// birthday_twin := strings.Contains(birthday, "March 21")
	// birthday_twin := strings.Contains(birthday, "March21")
	birthday_twin := strings.Contains(birthday, "199")
	fmt.Println(birthday_twin)

}
