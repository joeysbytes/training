package main

import (
	"fmt"
	"strings"
)

func main() {

	// draw a horizontal line
	heading := "Main Menu"
	line := strings.Repeat("-", len(heading))

	fmt.Println(line)
	fmt.Println(heading)
	fmt.Println(line)

}
