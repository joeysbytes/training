# Camp Code

## Description

A "how to program" course for beginners (also computer science 101). I'm mainly following along this course to
provide help where I can, and to learn Go.

## Information

| Item | Value |
| --- | --- |
| Start Date | 2022-05-02 |
| Overview | [Link](https://class.mastermnd.io/base-camp/camp-code) |
| Length | 22 Classes |
