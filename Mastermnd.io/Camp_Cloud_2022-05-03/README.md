# Camp Cloud

## Description

Provides cloud computing concepts and hands-on AWS experience. I am mainly taking this for the AWS experience, as I already
have the AWS Cloud Practitioner Certification, but am not getting any experience through work.

## Information

| Item | Value |
| --- | --- |
| Start Date | 2022-05-03 |
| Overview | [Link](https://class.mastermnd.io/base-camp/camp-cloud) |
| Length | 22 Classes |
