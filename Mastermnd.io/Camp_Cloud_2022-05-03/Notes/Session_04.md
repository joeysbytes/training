# Camp Cloud: Session 4 - 2022-05-31

**Note:** Class was cancelled on original scheduled date of: 2022-05-24

## AWS Labs (EC2)

### Misc Notes

* What is a jump host?
  * A central place where you can ssh to all other computers, and set up all the firewall / connection rules.
  * Also called a bastion host.

### Lab 1: EC2 Instance Recovery

* Following "User-Data" method shown [here](https://aws.amazon.com/premiumsupport/knowledge-center/user-data-replace-key-pair-ec2/)
* Spin up new EC2 instance
  * Amazon Linux, new key pair
* chmod 400 initial_key.pem
* ssh -i initial_key.pem ec2-user@44.204.29.135
* Follow steps on web page
  * You get a new IP address everytime you stop/start the instance
* **Other Options:**
  * Use EC2 Serial Console
  * AWS Systems Manager > Session Manager
  * EC2 Instance Connect

### Lab 2: Hosting a Website

* Start a brand new server (terminate old one from lab above)
* **Notes:** Web Servers and Web Traffic Management
  * What is a web server?
  * Apache, Nginx, MS IIS, Tomcat, etc.
  * What is HTTP? - port 80
  * What is HTTPS? - port 443
  * What is a protocol?
* Spin up new EC2 instance
  * Amazon Linux, existing key pair
  * Open HTTP/HTTPS ports in security group
* ssh in
* Follow guide [here](https://www.linuxshelltips.com/install-nginx-in-linux/)
  * Replace default website with [this](https://www.quackit.com/html/templates/download/bootstrap/business-2.zip)
    * Create /var/www/html, download and unzip file here
    * Edit: /etc/nginx/nginx.conf

### Lab 3: Adding Storage(EBS) to your EC2

* Follow the guide [here](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-using-volumes.html)
* Using existing web server from lab 2
* **Notes:**
  * New EBS volumes must be in same availability zone (us-east-1b)
  * EBS volumes can be detached / attached to other EC2 instances

## Homework Links

- [x] [How to Host a Website on NGINX Web Server](https://www.linuxshelltips.com/install-nginx-in-linux/)
- [x] [Business-2.zip](https://www.quackit.com/html/templates/download/bootstrap/business-2.zip)
- [x] [Make an Amazon EBS volume available for use on Linux](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-using-volumes.html)
- [x] [How can I connect to my Amazon EC2 instance if I lost my SSH key pair after its initial launch?](https://aws.amazon.com/premiumsupport/knowledge-center/user-data-replace-key-pair-ec2/)

## Other Homework (Optional)

- [ ] Review EC2 docs from previous sessions
- [ ] Look in to Elastic IP Addresses
- [x] Look up how to do this provisioning from the command line.
  * [Install AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
  * [Setup AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-quickstart.html)

## Other Links

* [Test File Downloads](https://testfiledownload.com/)
