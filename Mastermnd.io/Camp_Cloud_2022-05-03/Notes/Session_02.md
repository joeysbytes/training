# Camp Cloud: Session 2 - 2022-05-10

## Intro to IAM + Shared Responsibility Model

* Shared Responsibility Model
  * Security and compliance handled by AWS + You
  * AWS responsible for security __OF__ the cloud
  * You are responsible for security __IN__ the cloud
* IAM - Identity Access Management
  * Allows who, what, and how services are accessed and can interact in your AWS account.
  * Authentication - verifies who you are
  * Authorization - verifies what you are allowed to do
  * Rule of Least - Resources should only have enough access to do its job
* Root user vs IAM user
* IAM Users can access AWS in 3 ways
  * Console (username + password, with optional MFA)
  * Programmatically (access keys)
    * AWS CLI
    * SDK
* IAM Groups - collection of users
* IAM Roles - entity that defines a set of permissions.
  * Not associated with users or groups
  * You let users and groups to assume a role
* IAM Permissions - let you specify access to resources
  * default = no permissions
* IAM Policies - An entity, when attached to an identity or resource, defines their permissions
* ARN - Amazon Resource Name
* A user can have 2 sets of access keys
* Watched professor install AWS CLI, we'll get specific instructions later.
  * Google how to install AWS CLI v2, follow instructions
  * Then run aws configure
  * Then run aws sts get-caller-identity

## Follow Along

* Log in to AWS as root user
* Create a user
* Download csv of credentials
* Stream died just before we were going to create groups
* Create group
* Create role

## Homework Links

- [x] [Session 2 Slides](https://docs.google.com/presentation/d/1QtWPKtcQ_28As4f6WbieEAszZmPI8kgxkg89PbMHDfQ/edit?usp=drive_web&authuser=0)
- [x] [Official IAM Docs](https://docs.aws.amazon.com/iam/index.html)
- [x] [AWS IAM Core Concepts You NEED to Know](https://www.youtube.com/watch?v=_ZCTvmaPgao)
