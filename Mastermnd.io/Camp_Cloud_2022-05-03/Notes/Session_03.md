# Camp Cloud: Session 3 - 2022-05-17

## EC2 + Linux

* EC2 - Elastic Compute Cloud
  * virtualized compute capacity (different from VPS)
  * resizeable
  * scalable
  * resources
    * vCPU
    * RAM
    * Storage
* Instance Types
  * Class - what an instance type is good for
    * General Purpose
    * Compute Optimized
    * Memory Optimized
    * Storage Optimized
  * In each class, are many sizes
* Pricing Models - Everything you use / do in AWS will have a cost to it.
  * On Demand - you pay for what you use
    * Charged by hour or second
    * Most common, most expensive option
    * No minimums or committment
  * Reserved
    * Pre-pay EC2 compute capacity
    * Significant discount
    * 1 or 3 year committment
  * Spot
    * You set a price you are willing to pay for capacity.
    * You get when price falls below that threshold.
    * No committment, but capacity not guaranteed.
    * Under-utilized feature, not used effectively
    * Works like the stock market
  * Dedicated Hosts
    * Physical servers dedicated to you (no one else is on your server)
    * Very expensive option
* Storage on EC2
  * Instance Store
    * Block storage
    * Not permanent
    * Data goes away with instance
  * Object Storage - will learn this later with S3
* EBS - Elastic Block Storage
  * Does not go away with EC2 instance
  * Can mount multiple EBS volumes to a single EC2 in same AZ
    * Cannot mount same EBS volume to different EC2 instances
  * Pay only for what you use
  * Great for snapshot backups (point in time backup)
    * Snapshots stored as deltas, saves a lot of space
  * Types
    * SDD
    * HDD
    * Previous Generation
    * Many specific types within the above listed types
* AMI - Amazon Machine Image
  * read-only filesystem image
  * has OS and software to deliver a service
  * like VirtualBox OVA
* Resource Tagging
  * Metadata you add to your instances
  * Helps make resource management easier
  * Queryable
* Security Groups - Ports
  * This is not IAM?
  * Port is an endpoint to a logical connection and the way a client program specifies a specific server program on a computer in a network.
  * Only one service can use a port at a time
  * In TCP/IP and UDP networks
* CIDR Range - Classless Inter-Domain Routing
  * Method for allocating IP addresses and IP routing
  * Way to reserve a block of IP addresses
  * Example: 10.0.10.0/16 or 10.0.10.0/32
* Logging In
  * SSH - secure shell
    * No password login
    * Must use SSH key pairs to authenticate
    * Must use proper OS user
      * Amazon is terrible at telling you this
      * The user changes based on the AMI chosen
        * Amazon Linux - ec2-user
        * RedHat - root
        * Ubuntu - ubuntu
  * Security Groups
    * Virtual Firewall - control inbound / outbound traffic
    * A security group can be attached to multiple instances
* Elastic IPs: You keep an IP address that can be assigned to resources as they go up and down

## Follow Along

* Log in to AWS as root
* Choose a region (Ohio: us-east-2)
* Create EC2 Instance: Amazon Linux, t2.micro
  * Generate new ssh keys
* Log in via ssh
  * ssh -i sshKeyName userName@ipAddress
  * Have to fix key file permissions (unless on Windows): 400 (or 600)

## Homework Links

- [x] [Session 3 Slides](https://docs.google.com/presentation/d/1Ily-eElJ-eK9KhXOCXU5n03VfMxNH9HDxRJtsUQPK80/edit?usp=drive_web&authuser=0)
- [x] [Amazon EC2 Instance Types](https://aws.amazon.com/ec2/instance-types/)
- [x] [Amazon EBS volume types](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-volume-types.html)
- [x] [Manage user accounts on your Amazon Linux instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/managing-users.html) - List of user names created based on AMI
- [x] [Subnet Calculator](https://mxtoolbox.com/subnetcalculator.aspx)
- [x] [Easy Understanding of Web Protocols - HTTP and HTTPS](https://www.izooto.com/blog/understanding-http-https-protocols)
- [x] [NGINX Explained in 100 Seconds](https://www.youtube.com/watch?v=JKxlsvZXG7c)
- [x] [NGINX Linux Server | Common Configurations](https://www.youtube.com/watch?v=MP3Wm9dtHSQ)
- [x] [Proxy vs reverse proxy vs load balancer (2020) | Explained with real life examples](https://www.youtube.com/watch?v=MiqrArNSxSM)

## Other Links

* [Spot](https://spot.io/) - Helps effectively utilize AWS pricing models with Spot instances
* [Cloud Native Computing Foundation](https://www.cncf.io/)
* [A Cloud Guru](https://acloudguru.com/)
