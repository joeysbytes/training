# Camp Cloud: Session 1 - 2022-05-03

## Syllabus Day + Intro to Cloud/AWS

* Cloud = Someone else's computer
  * Distributed, networked computing resources
  * On-Demand access
    * No direct active management by user
* Computing resources
  * CPU
  * RAM
  * Hard Drive
  * Video Card
* Discussed old way of doing things (100% on premises)
* Going to learn with AWS
  * They are the market leader, but Azure is also a big player
  * Google Cloud Platform is playing 3rd place
* Hands on
  * Create aws account
  * Set up billing alerts
  * Should use new console layout
  * ~ 175 services and growing, categorized by the problems they are trying to solve
  * You can try to install aws CLI v2, but we'll do this later in class
* AWS global infrastructure
  * Regions: Physical locations in the world with clusters of data centers
    * How to choose
      * Latency
      * Available Services
      * Data laws / Requirements
    * ~ 26 Regions
  * Availability Zones (AZ): 1 or more data centers within a region
    * Redundant power, networking, connectivity
    * ~ 84 Availability Zones

## Homework Links

- [x] [Session 1 Slides](https://docs.google.com/presentation/d/1InmNdp16FZu5CEwQZZ8bnZkbW0_ImbAMSbUjx2NML4M/edit?usp=drive_web&authuser=0)

## Other Links

* Shows what to learn for specific jobs: [Roadmap.sh](https://roadmap.sh/)
