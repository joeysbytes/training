# Camp Cloud: Session 5 - 2022-06-08

## Load Balancing and Scaling

* Load Balancing
  * Improves distribution of workloads across multiple computing resources.
* Load Balancing Algorithms
  * Round Robin
    * Each request between servers sequentially (circularly)
  * Weighted Round Robin
    * Each server is given a weight
    * Round Robin adjusts accordingly based on weight
    * Use cases:
      * Latency reasons
      * Cost management
      * Servers are different sizes
  * Least Connection
    * Requests go to server with least number of connections
  * Weighted Least Connection
    * Each server is given a weight
  * Source IP Hash
    * Chooses server to send you to
    * Caches this decision, tries to keep user on same server
    * Can cause unbalanced loads, based on user activity
    * aka "Sticky Sessions"
  * (there are more)
* Server Pools / Target Groups
  * Pool - collection of origin servers your load balancer can send requests to
  * In AWS these are called "Targets"
  * You can add / delete servers from the target group as needed
* Health Checks
  * Attempts to open a TCP connection to an instance on a specified port.
  * Failure to connect, or timeout, is unhealthy
  * Unhealthy hosts will be removed from the pool
* Scaling and Scalability
  * "Increasing" the capacity to meet the "increasing" workload
* Elasticity
  * "Increasing or reducing" the capacity to meet the "increasing or reducing" workload.
* Types of Scaling
  * Vertical / Scaling Up
    * Adding resources / capacity to existing server(s)
  * Horizontal / Scaling Out
    * Adding more of the same size resources
    * Adding servers to the pool
  * Notes:
    * You can mix these scaling types together
* Auto Scaling
  * AWS monitors your applications, automatically adjusts capacity for predictable performance at lowest cost.
* ASG (Auto-Scaling Group) - Launch Configuration
  * Launch Configuration: a template that an auto scaling group uses to launch EC2 instances.
  * Auto Scaling Group: collection of EC2 instances treated as a logical grouping for purposes of automatic scaling and management.
* Dynamic Scaling
  * Can have target group scale based on many factors
    * CPU Utilization
    * Network in/out
    * ALB Requests (Application Load Balancers)
    * Custom options from Cloudwatch
  * Tuning this can be very tricky / takes practice

## Follow Along

* Load Balance Lab: Load Balancing with Nginx
  * 4 servers spun up, Amazon Linux
  * Rename 1 as load balancer, others as Web #
  * Log in to all 4
  * Run updates: sudo yum update -y
  * Install nginx: sudo amazon-linux-extras install nginx1 -y
  * Start nginx: sudo systemctl start nginx
  * Make each index page unique: sudo nano /usr/share/nginx/html/index.html
  * Update Nginx configuration on load balancer per docs: sudo nano /etc/nginx/nginx.conf
  * Test nginx configuration: sudo nginx -t
  * Reload nginx service: sudo systemctl reload nginx
  * Load Balancer IP address is now round-robin with the other servers
  * Terminate everything
  * Notes:
    * This is manual, you are in charge of it.
    * This does not scale
* Load Balance Lab: Using AWS
  * Create AWS load balancer (ALB)
  * Create a target group to attach to load balancer
  * Launch 4 EC2 instances, run updates, install nginx, start nginx, make index.html unique per server
    * Watch health target status
  * Go to DNS address of load balancer, will see it load balance sites
* Auto Scaling Lab
  * Create auto scaling group
    * Create launch template
    * Attach to load balancer
    * Set capacity limits

## Homework Links

- [x] [Session 5 Slides](https://docs.google.com/presentation/d/1uvxVvn_whjw4l1oR7Io5fkS0uN3X2stlT7DbZrGTV2E/edit?usp=drive_web&authuser=0)
- [x] [Using nginx as HTTP load balancer](http://nginx.org/en/docs/http/load_balancing.html)
- [x] [Target tracking scaling policies for Amazon EC2 Auto Scaling: Available Metrics](https://docs.aws.amazon.com/autoscaling/ec2/userguide/as-scaling-target-tracking.html#available-metrics)
